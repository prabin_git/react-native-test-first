import React, { Component } from 'react';
import CharImageWordView from './../CharImageWordView';

class ConsonantScreen extends Component {

    charSymbols = ['क', 'ख', 'ग', 'घ', 'ङ',
        'च', 'छ', 'ज', 'झ', 'ञ',
        'ट', 'ठ', 'ड', 'ढ', 'ण',
        'त', 'थ', 'द', 'ध', 'न',
        'प', 'फ', 'ब', 'भ', 'म',
        'य', 'र', 'ल', 'व', 'श', 'ष',
        'स', 'ह', 'क्ष', 'त्र', 'ज्ञ'];

    charWords = ['कमल', 'खरायो', 'गमला', 'घर', '',
        'चरा', 'छाता', 'जहाज', 'झरणा', '',
        'टमाटर', 'ठेकी', 'डमरू', 'ढक', '',
        'तबला', 'थपडी', 'दमकल', 'धरहरा', 'नरिवल',
        'परेवा', 'फलफूल', 'बस', 'भालु', 'मकै',
        'याक', 'रकेट', 'लसुन', 'वकिल', 'शलगम', 'षट्कोण',
        'सयपत्री', 'हलो', 'क्षत्रीय', 'त्रीशूल', 'ज्ञानी']

    charImages = [
        require('./imgs/a1.png'), require('./imgs/a2.png'), require('./imgs/a3.png'), require('./imgs/a4.png'),
        require('./imgs/a5.png'), require('./imgs/a6.png'), require('./imgs/a7.png'), require('./imgs/a8.png'),
        require('./imgs/a9.png'), require('./imgs/a10.png'), require('./imgs/a11.png'), require('./imgs/a12.png'),
        require('./imgs/a13.png'), require('./imgs/a14.png'), require('./imgs/a15.png'), require('./imgs/a16.png'),
        require('./imgs/a17.png'), require('./imgs/a18.png'), require('./imgs/a19.png'), require('./imgs/a20.png'),
        require('./imgs/a21.png'), require('./imgs/a22.png'), require('./imgs/a23.png'), require('./imgs/a24.png'),
        require('./imgs/a25.png'), require('./imgs/a26.png'), require('./imgs/a27.png'), require('./imgs/a28.png'),
        require('./imgs/a29.png'), require('./imgs/a30.png'), require('./imgs/a31.png'), require('./imgs/a32.png'),
        require('./imgs/a33.png'), require('./imgs/a34.png'), require('./imgs/a35.png'), require('./imgs/a36.png')]

    render() {

        return (
            <CharImageWordView
                charSymbols={this.charSymbols}
                charWords={this.charWords}
                charImages={this.charImages}
                soundFileInitial='a'>
            </CharImageWordView>
        );
    }
}

export default ConsonantScreen;
