import React, { Component } from 'react';

import { StyleSheet, View, Text } from 'react-native';
import MyGrid from './../GridViewDisplay'

const vegetables = [
    { id: 'brinjal', title: 'भ्यान्टा', imageUri: require('./imgs/brinjal.png') },
    { id: 'cabbage', title: 'बन्दा', imageUri: require('./imgs/cabbage.png') },
    { id: 'cauliflower', title: 'काउली', imageUri: require('./imgs/cauliflower.png') },
    { id: 'potato', title: 'अालु', imageUri: require('./imgs/potato.png') },
    { id: 'tomato', title: 'टमाटर', imageUri: require('./imgs/tomato.png') }
];

class VegetableScreen extends Component {
ञ
    render() {
        return (
            <View>
                <MyGrid data={vegetables} />
            </View>
        )
    }
}

export default VegetableScreen;