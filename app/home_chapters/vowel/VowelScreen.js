import React, { Component } from 'react';
import CharImageWordView from './../CharImageWordView';

class ConsonantScreen extends Component {

    charSymbols = ['अ', 'अा', 'इ', 'ई', 'उ', 'ऊ',
        'ऋ', 'ए', 'ऐ', 'अाे', 'अाै', 'अं', 'अः'];

    charWords = ['अनार', 'अामा', 'इन्द्रेणी', 'ईव्श्रर', 'उल्लु', 'ऊन',
        'ऋषि', 'एकतारे', 'ऐना', 'अाेखर', 'अाैषधी', 'अंगुर', 'अः'];

    charImages = [
        require('./imgs/b1.png'), require('./imgs/b2.png'), require('./imgs/b3.png'), require('./imgs/b4.png'),
        require('./imgs/b5.png'), require('./imgs/b6.png'), require('./imgs/b7.png'), require('./imgs/b8.png'),
        require('./imgs/b9.png'), require('./imgs/b10.png'), require('./imgs/b11.png'), require('./imgs/b12.png'),
        require('./imgs/b13.png')];

    render() {
        return (
            <CharImageWordView
                charSymbols={this.charSymbols}
                charWords={this.charWords}
                charImages={this.charImages}
                soundFileInitial='b'>
            </CharImageWordView>
        );
    }
}

export default ConsonantScreen;
