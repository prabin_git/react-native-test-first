import React, { Component } from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import Sound from 'react-native-sound';

class CharImageWordView extends Component {
    charSymbols = this.props.charSymbols;
    charWords = this.props.charWords;
    charImages = this.props.charImages;
    soundFileInitial = this.props.soundFileInitial;
    sound = new Sound(this.soundFileInitial + (1) + '.m4a', Sound.MAIN_BUNDLE);

    constructor(props) {
        super(props);
        i = 0;
        this.playSound();
    }

    loadPrevious = () => {
        if (--i < 0) i = 0;
        this.setState({ i })
        this.playSound();
    }

    loadNext = () => {
        if (++i >= this.charSymbols.length) i = 0;
        this.setState({ i })
        this.playSound();
    }

    playSound = () => {
        this.sound.stop();
        this.sound.release();

        var file = this.soundFileInitial + (i + 1) + '.m4a';
        console.log(file);
        this.sound = new Sound(file, Sound.MAIN_BUNDLE, (error) => {
            if (error) { return; }
            this.sound.play((success) => {
                //release audio player resource, otherwise audio won't play after multiple plays
                if (success) this.sound.release();
            });
        });
    }

    render() {
        var character = this.charSymbols[i];
        var word = this.charWords[i];
        var image = this.charImages[i];
        return (
            <View style={styles.contianer}>

                <Text style={styles.textCharacter}>{character}</Text>

                <View style={styles.horizontalContainer}>
                    <TouchableOpacity style={styles.buttonLR} onPress={this.loadPrevious} activeOpacity={0.4}>
                        <Image source={require('./shape_image/buttonLeftWhite.png')} />
                    </TouchableOpacity>
                    <Image source={image} />

                    <TouchableOpacity style={styles.buttonLR} onPress={this.loadNext} activeOpacity={0.4}>
                        <Image source={require('./shape_image/buttonRightWhite.png')} />
                    </TouchableOpacity>
                </View>

                <Text style={styles.textWord}>{word}</Text>
            </View>
        );
    }
}

export default CharImageWordView;

const styles = StyleSheet.create({
    contianer: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
        backgroundColor: '#fff'
    },
    horizontalContainer: {
        flex: .5,
        flexDirection: 'row',
        backgroundColor: 'powderblue',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    image: {
        width: '100%'
    },
    textCharacter: {
        flex: .25,
        fontSize: 100,
        alignSelf: 'center',
    },
    textWord: {
        flex: .25,
        fontSize: 50,
        alignSelf: 'center',
    },
    buttonLR: {
        opacity: 0.8
    }
});