import React, { Component } from 'react';

import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, BackHandler, Button } from 'react-native';
import Sound from 'react-native-sound';
import Dimensions from 'Dimensions';

const lyrics =
    'सयौं थुँगा फूलका हामी, एउटै माला नेपाली\n' +
    'सार्वभौम भई फैलिएका, मेची-माहाकाली।\n\n' +

    'सयौं थुँगा फूलका हामी, एउटै माला नेपाली\n' +
    'सार्वभौम भई फैलिएका, मेची-माहाकाली।\n\n' +

    'प्रकृतिका कोटी-कोटी सम्पदाको आंचल\n' +
    'वीरहरूका रगतले, स्वतन्त्र र अटल।\n\n' +

    'ज्ञानभूमि, शान्तिभूमि तराई, पहाड, हिमाल\n' +
    'अखण्ड यो प्यारो हाम्रो मातृभूमि नेपाल।\n\n' +

    'बहुल जाति, भाषा, धर्म, संस्कृति छन् विशाल\n' +
    'अग्रगामी राष्ट्र हाम्रो, जय जय नेपाल।';


const sound = new Sound('anthem.mp3', Sound.MAIN_BUNDLE);

class AnthemScreen extends Component {

    constructor() {
        super();
        this.playAudio();
    }

    playAudio() {

        sound.play((success) => {
            if (success) sound.release();
        })

    }

    render() {
        return (
            <ImageBackground style={styles.container} source={require('./imgs/flag.png')}>
                <View style={styles.lyricsContainer}>
                    <Text style={styles.text}>{lyrics}</Text>
                </View>
            </ImageBackground>
        )
    }

    componentDidMount() { BackHandler.addEventListener('hardwareBackPress', this.handleBackPress); }

    componentWillUnmount() { BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress); }

    handleBackPress = () => {
        sound.stop();
    }

    static navigationOptions = ({ navigation }) => (

        handleNavigateHome = (navigation) => {
            sound.stop();
            navigation.navigate('home');
        },
        {
            headerLeft: (
                <TouchableOpacity activeOpacity={0.5} onPress={() => handleNavigateHome(navigation)}>
                    <Image style={{ height: 20, width: 20, marginLeft: 15 }} source={require('./../shape_image/buttonClose.png')}></Image>
                </TouchableOpacity>
            ),
        })
}

export default AnthemScreen;

const styles = StyleSheet.create({
    scrollContainer: {
        justifyContent: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    lyricsContainer: {
        backgroundColor: '#fff',
        opacity: 0.8,
        padding: 16,
        borderRadius: 10,
        justifyContent: 'center'
    },
    text: {
        fontSize: 20,
        textAlign: 'center',
        color: '#000'
    }
})
