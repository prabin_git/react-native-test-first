import React, { Component } from 'react';
import { StyleSheet, TouchableOpacity, Image, Text, View } from 'react-native';
import GridView from 'react-native-gridview';
import Sound from 'react-native-sound';
import Lightbox from 'react-native-lightbox';
var sound;

function CustomGridLayout(props) {
    const data = props.data;

    itemsPerRow = 2;
    const randomData = [];
    for (let i = 0; i < data.length; i) {
        const endIndex = Math.max(Math.round(Math.random() * itemsPerRow), 0) + i;
        randomData.push(data.slice(i, endIndex));
        i = endIndex;
    }

    const dataSource = new GridView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2,
    }).cloneWithRows(randomData);

    return (
        <GridView
            data={data}
            dataSource={true ? dataSource : null} /*When true,uses dataSource containing varying item count in each row*/
            itemsPerRow={itemsPerRow}

            renderItem={(item) => {
                return (
                    <GridItem gridItem={item} />
                );
            }}
        />
    );
}

export default CustomGridLayout;

class GridItem extends Component {

    render({ gridItem } = this.props) {
        const { id, title, imageUri } = gridItem;
        return (
            <Lightbox
                springConfig={{ tension: 300, friction: 40 }}
                swipeToDismiss={false}
                backgroundColor={'#fff'}
                underlayColor={'#fff'}

                renderHeader={close => (
                    <TouchableOpacity onPress={close}>
                        <Image source={require('./shape_image/buttonClose.png')} style={styles.closeButton} />
                    </TouchableOpacity>
                )}

                renderContent={() =>
                    <View style={styles.fullScreenContainer}>
                        <Image source={imageUri} style={styles.imageFull} />
                        <Text style={styles.textFull}>{title}</Text>
                    </View>
                }

                didOpen={() => this.playAudio(id)}
                onClose={() => {
                    sound.stop();
                    sound.release();
                }}
                >
                <View style={styles.container}>
                    <Image source={imageUri} style={styles.image} />
                    <Text style={styles.text}>{title}</Text>
                </View>

            </Lightbox>
        )
    }

    playAudio(id) {
        try {
            sound.stop();
            sound.release();
        } catch (err) { };
        file = 'sound_' + id + '.mp3';
        sound = new Sound(file, Sound.MAIN_BUNDLE, error => {
            if (error) return;
            sound.play((success) => {
                if (success) sound.release();
            });
        })
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 2,
        borderRadius: 4,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
        elevation: 2    
    },
    image: {
        height: 150,
        width: '100%',
        resizeMode: 'contain'
    },
    text: {
        fontSize: 24,
        textShadowColor: '#000',
        textShadowRadius: 20,
        justifyContent: 'center',
        padding: 4,
    },
    fullScreenContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    imageFull: {
        flex: 0.8,
        width: '100%',
        resizeMode: 'contain',
        marginTop: 20
    },
    textFull: {
        flex: 0.1,
        fontSize: 18,
        color: '#6f7878',
        justifyContent: 'center',
        textAlign: 'center',
        position: 'absolute',
        bottom: 10,
        left: 0,
        width: '100%'
    },
    closeButton: {
        height: 30,
        width: 30,
        marginTop: 12,
        marginLeft: 12,
        resizeMode: 'contain'
    }
});
