import React, { Component } from 'react';

import {StyleSheet, View, Text, Image, ImageBackground, TouchableOpacity } from 'react-native';
import Sound from 'react-native-sound';

class ImageWordView extends Component {

    dataList = this.props.dataList;
    images = this.props.dataImages;
    soundFileInitial = this.props.soundFileInitial;
    sound = new Sound(this.soundFileInitial + '1.m4a', Sound.MAIN_BUNDLE);

    constructor(props) {
        super(props);
        i = 0;
        this.playSound();
    }

    loadPrevious = () => {
        if (--i < 0) i = 0;
        this.setState({ i });
        this.playSound();
    }

    loadNext = () => {
        if (++i >= this.dataList.length) i = 0;
        this.setState({ i });
        this.playSound();
    }

    playSound = () => {
        this.sound.stop();
        this.sound.release();

        var file = this.soundFileInitial + (i + 1) + '.m4a';
        this.sound = new Sound(file, Sound.MAIN_BUNDLE, (error) => {
            if (error) { return; }
            this.sound.play((success) => {
                if (success) this.sound.release();
            });
        });
    }

    render() {
        var data = this.dataList[i];
        var image = this.images[i];

        return (
            <ImageBackground style={styles.imageContainer} source={image} >
                <View style={styles.horizontalRow}>
                    <TouchableOpacity style={styles.buttonLR} onPress={this.loadPrevious}>
                        <Image source={require('./shape_image/buttonLeftWhite.png')} />
                    </TouchableOpacity>
                    <View style={styles.textContainer}>
                        <Text style={styles.text}>{data}</Text>
                    </View>
                    <TouchableOpacity style={styles.buttonLR} onPress={this.loadNext} >
                        <Image source={require('./shape_image/buttonRightWhite.png')} />
                    </TouchableOpacity>
                </View>
            </ImageBackground>
        )
    }
}

export default ImageWordView;

const styles = StyleSheet.create({
    imageContainer: {
        height: '100%',
        width: '100%'   
    },
    horizontalRow: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    textContainer: {
        backgroundColor: '#fff',
        borderRadius: 10,
        paddingTop: 5,
        paddingBottom: 5,
        paddingLeft: 25,
        paddingRight: 25,
        opacity: 0.7
    },
    text: {
        fontSize: 48,
        color: '#db550d',   
    },
    buttonLR: {
        opacity: 0.8
    }
})