import React, { Component } from 'react';

import { StyleSheet, View, Button, Text, Image, ToastAndroid, TouchableHighlight, Alert } from 'react-native';

import { Actions } from 'react-native-router-flux';

import data from './DataNamePicture';

var isShuffled;
var shuffledButton;

class NamePicture extends Component {

    constructor() {
        super();
        inputState = [];
        question = [];
        isShuffled = false;
        shuffledButton = [];

        this.state = ({
            score: 0,
            i: Math.floor(Math.random() * data.length),
            inputIndex: 0,
            questionsCount: 1
        });
    }

    updateUserInput(char) {
        //ToastAndroid.show(char + '', ToastAndroid.SHORT);
        question = data[this.state.i].name;
        i = this.state.inputIndex;
        inputState[i] = char;

        i++;
        this.setState({
            inputIndex: i,
        });


        if (inputState.length !== question.length) {
            return;
        }
        //check answer and update score
        if (inputState.toString() == question.toString()) {
            //ToastAndroid.show('Correct', ToastAndroid.SHORT);
            setTimeout(() => {
                inputState = [];
                isShuffled = false;
                newScore = this.state.score + 2;
                qCount = this.state.questionsCount + 1;
                this.setState({
                    inputIndex: 0,
                    score: newScore,
                    questionsCount: qCount,
                    i: Math.floor(Math.random() * data.length),
                });
            }, 1000);
        } else {
            ToastAndroid.show('\'' + inputState.toString() + '\'' + ' मिलेन', ToastAndroid.SHORT);
            setTimeout(() => {
                inputState = [];
                newScore = this.state.score - 1;
                this.setState({
                    inputIndex: 0,
                    score: newScore,
                });
            }, 1000);
        }

        if (this.state.questionsCount >= 15) {
            //score doesn't get uploaded till 1200ms
            setTimeout(() => {
                this.finishGame();
            }, 1200);
        }
    }

    finishGame = () => {
        Alert.alert(
            '',
            'प्राप्ताङ्क : ' + this.state.score,
            [
                {
                    text: 'फेरी खेल्ने', onPress: () => {
                        this.setState({
                            inputIndex: 0,
                            score: 0,
                            questionsCount: 1,
                        })
                    }
                },
                {
                    text: 'बन्द गर', onPress: () => {
                        Actions.pop();
                    }
                }
            ]
        )
    }

    render() {
        word = data[this.state.i].name;

        buttons = [];
        inputs = [];

        for (n = 0; n < word.length; n++) {
            buttons.push({ index: n, key: 'btn' + n, char: word[n] });
            inputs.push({ index: n, key: 'in_' + n });
        }
        buttons = this.shuffle(buttons);

        renderButtons = buttons.map(btn => {
            return <TouchableHighlight
                style={{ height: 60, width: 60, backgroundColor: '#336688', margin: 2, elevation: 2, borderRadius: 2 }}
                key={btn.key}
                onPress={() => this.updateUserInput(btn.char)}
                underlayColor='#223344'
            >
                <Text style={{
                    fontSize: 36, color: '#fff', textAlign: 'center', textAlignVertical: 'center'
                }}>{btn.char}</Text>
            </TouchableHighlight>;
        });

        renderUserInputs = inputs.map(n => {
            return <Text
                style={{
                    height: 50, width: 50, fontSize: 24, backgroundColor: '#77ff77', margin: 2,
                    textAlign: 'center', textAlignVertical: 'center', borderRadius: 2
                }}
                key={n.key}>{inputState[n.index]}</Text>;
        });

        return (
            <View style={styles.container}>
                <View style={{ flexDirection: 'row', width: '100%' }}>
                    <Text style={{ flex: 1, fontSize: 20, color: '#222', textAlign: 'center' }}>
                        {'प्रश्न नं : ' + this.state.questionsCount}</Text>
                    <Text style={{ flex: 1, fontSize: 20, color: '#222', textAlign: 'center' }}>
                        {'प्राप्ताङ्क : ' + this.state.score}</Text>
                </View>

                <View style={styles.questionContainer}>
                    <Image source={data[this.state.i].image} style={styles.image} />
                    <View style={{ flexDirection: 'row' }}>
                        {renderButtons}
                    </View>
                    <View style={{ flexDirection: 'row' }}>
                        {renderUserInputs}
                    </View>
                </View>
            </View>
        )
    }

    shuffle = (array) => {
        if (isShuffled) {
            return shuffledButton;
        }
        var currentIndex = array.length, temporaryValue, randomIndex;

        // While there remain elements to shuffle...
        while (0 !== currentIndex) {

            // Pick a remaining element...
            randomIndex = Math.floor(Math.random() * currentIndex);
            currentIndex -= 1;

            // And swap it with the current element.
            temporaryValue = array[currentIndex];
            array[currentIndex] = array[randomIndex];
            array[randomIndex] = temporaryValue;
        }

        isShuffled = true;
        shuffledButton = array;

        return array;
    }
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#EFEAA8',
        flex: 1,
    },
    questionContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        resizeMode: 'contain',
        marginLeft: 10,
        marginRight:10
    },
    image: {
        flex: 0.7,
        width: '100%',
        resizeMode: 'contain'
    },
    timerText: {
        fontSize: 20,
        color: '#222'
    }
});

export default NamePicture;


