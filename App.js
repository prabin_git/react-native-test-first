import React, { Component } from 'react';

import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated', 'Module RCTImageLoader']);

import {Router, Scene} from 'react-native-router-flux';

import InitialScreen from './app/InitialScreen';

import HomeScreen from './app/home/HomeScreen';
import ConsonantScreen from './app/home_chapters/consonant/ConsonantScreen';
import VowelScreen from './app/home_chapters/vowel/VowelScreen';
import DayScreen from './app/home_chapters/day/DayScreen';
import MonthScreen from './app/home_chapters/month/MonthScreen';
import SeasonScreen from './app/home_chapters/season/SeasonScreen';
import FruitScreen from './app/home_chapters/fruit/FruitScreen';
import VegetableScreen from './app/home_chapters/vegetable/VegetableScreen';
import AnimalScreen from './app/home_chapters/animal/AnimalScreen';
import BirdScreen from './app/home_chapters/bird/BirdScreen';
import AnthemScreen from './app/home_chapters/anthem/AnthemScreen';

import GameMenu from './app/game/GameMenu';

import NamePicture from './app/game/namePicture/NamePicture';

import PairPicture from './app/game/pairPicture/PairPicture';


const app = () => {
  return (
    <Router>
      <Scene key='root'>

        <Scene key='initial'
        component={InitialScreen}
        title=''
        hideNavBar={true}
        initial/>

        <Scene key='home'
        component={HomeScreen}
        title='पढार्इ सिकार्इ'/>

        <Scene key='consonant'
        component={ConsonantScreen}
        title='व्यञ्नजन वर्ण'/>

        <Scene key='vowel'
        component={VowelScreen}
        title='स्वर वर्ण'/>

        <Scene key='day'
        component={DayScreen}
        title='दिन'/>

        <Scene key='month'
        component={MonthScreen}
        title='महिना'/>

        <Scene key='season'
        component={SeasonScreen}
        title='ऋतुहरू'/>

        <Scene key='fruit'
        component={FruitScreen}
        title='फलफुल'/>

        <Scene key='vegetable'
        component={VegetableScreen}
        title='तरकारी'/>

        <Scene key='animal'
        component={AnimalScreen}
        title='जनावरहरू'/>

        <Scene key='bird'
        component={BirdScreen}
        title='चराचुरुङ्गी'/>

        <Scene key='anthem'
        component={AnthemScreen}
        title='राष्ट्रिय गान'/>
        
        <Scene key='games'
        component={GameMenu}
        title='मनोरञ्जन'/>

        <Scene key='namePicture'
        component={NamePicture}
        title='चित्र चिन'/>

        <Scene key='pairPicture'
        component={PairPicture}
        title='सम्झेर खेल'/>       

      </Scene>
    </Router>
  );
}
export default app;